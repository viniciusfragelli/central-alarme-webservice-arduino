#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 1, 177);
byte gateway[] = {192,168,1,87};
byte submask[] = {255,255,255,0};
EthernetServer server(80);
boolean status_alarme;
boolean status_disparado;
String urls;

#define pino_alarm 3
#define pino_cirene 4

void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac, ip, gateway, submask);
  pinMode(pino_alarm, INPUT);
  pinMode(pino_cirene, OUTPUT);
  digitalWrite(pino_cirene,HIGH);
  status_alarme = false;
  status_disparado = false;
  urls = "";
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}


void loop() {
  webServiceAlarme();
  leEstadoAlarme();
}

void leEstadoAlarme(){
  if(status_alarme && !status_disparado){
    Serial.print("Stado do alarme: ");
    Serial.println(digitalRead(pino_alarm));
    if(digitalRead(pino_alarm) == 0){
      status_disparado = true;
      digitalWrite(pino_cirene,LOW);
    }
  }
}

void webServiceAlarme(){
  EthernetClient client = server.available();
  if (client) {
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if (c == '\n') {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          //client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("{");
          client.print("'alarme': '");
          client.print(status_alarme);
          client.println("',");
          client.print("'disparado': '");
          client.print(status_disparado);
          client.println("'");
          client.println("}");

          acaoURL();
          
          break;
        }
        if(urls.length() < 100){
          urls += c;
        }
      }
    }
    delay(10);
    client.stop();
    }  
}

void acaoURL(){
  Serial.println(urls);
  if(urls.indexOf("?ligar") > 0){
    ligaAlarme();
  }
  if(urls.indexOf("?desligar") > 0){
    desligarAlarme();
  }
  if(urls.indexOf("?panico") > 0){
    panicoAlarme();
  }
  urls = "";
}

void ligaAlarme(){
  digitalWrite(pino_cirene,LOW);
  delay(300);
  digitalWrite(pino_cirene,HIGH);

  status_alarme = true;
  digitalWrite(pino_cirene,HIGH);
  Serial.println("Ligar");
}

void desligarAlarme(){
  digitalWrite(pino_cirene,LOW);
  delay(300);
  digitalWrite(pino_cirene,HIGH);
  delay(300);
  digitalWrite(pino_cirene,LOW);
  delay(300);
  digitalWrite(pino_cirene,HIGH);

  status_alarme = false;
  status_disparado = false;
  digitalWrite(pino_alarm,HIGH);
  Serial.println("Desligar");
}

void panicoAlarme(){
  status_alarme = true;
  status_disparado = true;  
  digitalWrite(pino_cirene,LOW);
  Serial.println("Panico");
}

